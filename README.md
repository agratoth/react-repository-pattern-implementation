# React Repository Pattern Implementation


### Install
```bash
npm i react react-dom
npm i react-router-dom@6
npm i axios
npm i redux react-redux redux-saga @types/react-redux @types/redux-saga
npm i --save-dev webpack webpack-cli webpack-dev-server
npm i --save-dev typescript ts-loader @types/react @types/react-dom
npm i --save-dev babel-loader css-loader style-loader html-webpack-plugin
npm i --save-dev @babel/core @babel/preset-env @babel/preset-react
npm i --save-dev sass sass-loader sass-resources-loader css-modules-typescript-loader
```