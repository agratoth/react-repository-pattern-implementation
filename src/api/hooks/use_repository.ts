import { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { IBaseRepository, IModel, IRepositoryConstructor } from 'api/models/base';
import { TBaseState, TModelState, TState } from 'api/redux';

export const useRepository = <M extends IModel>(r: IRepositoryConstructor<M>): IBaseRepository<M> => {
  const repo = useMemo(() => {
    return new r();
  }, [])

  repo.dispatch = useDispatch();
  repo.state = useSelector(
    (state: TState<M, string>) => state.models[repo.name]
  );

  useEffect(() => {
    repo.init();
  }, []);

  return repo;
};