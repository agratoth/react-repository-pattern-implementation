import { Dispatch } from 'react';

import { Actions, initialMethodState, initialModelState, Methods, TAction, TMethodState, TModelState } from 'api/redux';

export interface IModel {
  object_id?: string;
}

export interface IRepositoryConstructor<M> {
  new(): IBaseRepository<M>;
}

export interface IBaseRepository<M> {
  name: string;
  state: TModelState<M, string>;

  dispatch: Dispatch<any>;

  get findState(): TMethodState<M[], string>;
  getState(): TModelState<IModel, string>;

  save(data: M): void;
  get(object_id: string): void;
  find(): TMethodState<M[], string>;
  delete(object_id: string): void;
  init(): void;
}

export class APIRepository<M extends IModel> implements IBaseRepository<M> {
  name: string;
  api: string;
  dispatch: Dispatch<any>;
  state: TModelState<M, string>;

  get findState(): TMethodState<M[], string> {
    if (this.state && this.state.find) {
      return this.state.find;
    }

    return {
      ...initialMethodState,
      data: [],
    }
  }

  initAction(): TAction {
    return {
      type: Actions.INIT,
      model: this.name,
    };
  }

  findAction(): TAction {
    return {
      type: Actions.REQUEST,
      model: this.name,
      method: Methods.FIND,
      api: this.api,
    };
  }

  getState(): TModelState<IModel, string>{
    if (this.state) {
      return this.state
    }

    return initialModelState;
  }

  init() {
    this.dispatch(this.initAction());
  }

  save(data: M) {
  }

  get(object_id: string) {
  }

  find(): TMethodState<M[], string> {
    this.dispatch(this.findAction());

    //if (this.state && this.state.find) {
    //  return this.state.find;
    //}

    return {
      ...initialMethodState,
      data: [],
    }
  }

  delete(object_id: string) {
  }
}