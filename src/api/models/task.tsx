import { APIRepository, IModel } from './base';

export enum TaskStatuses {
  NEW = 'new',
  IN_PROGRESS = 'in_progress',
  BLOCKED = 'blocked',
  DONE = 'done'
}

export interface ITask extends IModel {
  name: string;
  description: string;
  status: TaskStatuses;

  assignee_id: string;
}

export class TaskRepository extends APIRepository<ITask>{
  constructor() {
    super();

    this.name = 'tasks';
    this.api = 'https://62161b377428a1d2a35b5661.mockapi.io/tasks/'   
  }
}