import { IModel } from 'api/models/base';

export enum Actions {
  REQUEST = 'request',
  SUCCESS = 'success',
  ERROR = 'error',
  CLEANUP = 'cleanup',
  INIT = 'init',
}

export enum Methods {
  FIND = 'find',
  SAVE = 'save',
  GET = 'get',
  DELETE = 'delete',
}

export type TAction = {
  type: Actions;
  model: string;
  method?: Methods;
  api?: string;
  payload?: IModel[];
};

export type TMethodState<M, E> = {
  pending: boolean;
  data: M;
  error: E | null;
};

export type TModelState<M, E> = {
  [Methods.FIND]: TMethodState<M[], E>;
  [Methods.DELETE]: TMethodState<boolean, E>;
  [Methods.GET]: TMethodState<Record<string, M>, E>;
  [Methods.SAVE]: TMethodState<M | null, E>;
}

export type TState<M, E> = {
  models: Record<string, TModelState<M, E>>;
};

export type TBaseState = TState<IModel, string>;

const initialState: TBaseState = {
  models: {},
}

export const initialMethodState = {
  pending: false,
  error: null,
}

export const initialModelState: TModelState<IModel, string> = {
  [Methods.FIND]: {
    ...initialMethodState,
    data: [],
  },
  [Methods.DELETE]: {
    ...initialMethodState,
    data: false,
  },
  [Methods.GET]: {
    ...initialMethodState,
    data: {},
  },
  [Methods.SAVE]: {
    ...initialMethodState,
    data: null,
  },
};

export const reducer = (state=initialState, action: TAction): TBaseState => {
  switch (action.type) {
    case Actions.INIT:
      return {
        ...state,
        models: {
          ...state.models,
          [action.model]: initialModelState,
        }
      };
    case Actions.REQUEST:
      return {
        ...state,
        models: {
          ...state.models,
          [action.model]: {
            ...state.models[action.model],
            [action.method!]: {
              ...state.models[action.model][action.method!],
              pending: true,
            }
          }
        }
      };
    case Actions.SUCCESS:
      return {
        ...state,
        models: {
          ...state.models,
          [action.model]: {
            ...state.models[action.model],
            [action.method!]: {
              ...state.models[action.model][action.method!],
              pending: false,
              data: action.payload,
              error: null,
            }
          }
        }
      };
  }
  return state;
};