import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import { IModel } from 'api/models/base';

import { TAction, Actions, Methods } from './reducer';

export const getClient = (): AxiosInstance => {
  return axios.create();
};

const findRequest = (api: string) => {
  const apiClient = getClient();

  return apiClient.get<IModel[]>(api);
};

const findSuccess = (action: TAction, payload: IModel[]): TAction => {
  return {
    type: Actions.SUCCESS,
    model: action.model,
    method: action.method,
    payload: payload,
  };
};

export function* requestSaga(action: TAction): Generator {
  try{
    switch(action.method!){
      case Methods.FIND:
        const response = yield call(findRequest, action.api!);
        yield put(
          findSuccess(action, (response as AxiosResponse<IModel[]>).data)
        );
    }
  } catch(e) {
    
  }
}

export function* saga(): Generator {
  yield all([
    takeLatest(Actions.REQUEST, requestSaga),
  ]);
}
