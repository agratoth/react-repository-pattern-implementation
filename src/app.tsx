import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { store } from 'api/store';

import { MainPage } from 'pages/main';

const App = (): JSX.Element => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={ <MainPage /> } />
        </Routes>
      </BrowserRouter>
    </Provider>
    
  );
};

export default App;