import React, { useEffect } from 'react';

import { useRepository } from 'api/hooks';
import { TaskRepository } from 'api/models/task';

import styles from './main.module.scss';

export const MainPage = ():JSX.Element => {
  const repo = useRepository(TaskRepository);
  const {pending, data: tasks, error} = repo.findState;

  const rows = tasks.map(m => {
    return (
      <tr key={ m.object_id }>
        <td className={ styles.id_cell }>{ m.object_id }</td>
        <td>{ m.name }</td>
        <td>{ m.description }</td>
        <td className={ styles.status_cell }>{ m.status }</td>
      </tr>
    );
  });

  useEffect(() => {
    repo.find();
  }, []);

  return (
    <div className={ styles.container }>
      <table>
        <thead>
          <tr>
            <th className={ styles.id_column }>ID</th>
            <th className={ styles.name_column }>Задача</th>
            <th className={ styles.description_column }>Описание</th>
            <th className={ styles.status_column }>Статус</th>
          </tr>
        </thead>
        <tbody>
          { rows }
        </tbody>
      </table>
    </div>    
  );
};