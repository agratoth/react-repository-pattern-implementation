import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';
import 'styles/globals.scss';

ReactDOM.render(<App />, document.getElementById('main'));